package com.zgj.demojetpackapp.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.bean.Student

import kotlinx.android.synthetic.main.activity_test_view_model.*

/**
 *  @author Simple
 *  @date 2020/1/6
 *  @description ：
 **/
class TestActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_view_model)

        val model = ViewModelProviders.of(this)[MyViewModel::class.java]
        model.getStudent().observe(this, Observer{ student ->
            Log.d(Common.TAG,"name:${student.name}")
            Log.d(Common.TAG,"age:${student.age}")
            tv_name.text = student.name
            val age = Integer.toString(student.age)
            tv_age.text= age
        })
        tv_action.setOnClickListener {
            val liveData:MutableLiveData<Student> = model.getStudent() as MutableLiveData<Student>
            liveData.value = Student("小帅哥",8)
        }
    }

}