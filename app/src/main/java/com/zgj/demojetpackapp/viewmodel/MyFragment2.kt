package com.zgj.demojetpackapp.viewmodel

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.bean.Student
import kotlinx.android.synthetic.main.fragment2_test_view_model.*

/**
 *  @author Simple
 *  @date 2020/1/8
 *  @description ：
 **/
class MyFragment2 :Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment2_test_view_model,container,false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val model = ViewModelProviders.of(activity!!)[MyViewModel::class.java]
        model.getStudent().observe(this, Observer{ student ->
            Log.d(Common.TAG,"MyFragment2->name:${student.name}")
            Log.d(Common.TAG,"MyFragment2->age:${student.age}")
            tv_name.text = student.name
            val age = Integer.toString(student.age)
            tv_age.text= age
        })
        tv_action.setOnClickListener {
            val liveData: MutableLiveData<Student> = model.getStudent() as MutableLiveData<Student>
            liveData.value = Student("小奶狗", 22)
        }

    }



}