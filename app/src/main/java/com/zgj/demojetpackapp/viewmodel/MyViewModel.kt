package com.zgj.demojetpackapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zgj.demojetpackapp.bean.Student
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.properties.ReadOnlyProperty
import kotlin.contracts.contract as contract1

/**
 *  @author Simple
 *  @date 2020/1/7
 *  @description ：
 **/
class MyViewModel : ViewModel() {


    private val student: MutableLiveData<Student> by lazy{
        //        val liveData = MutableLiveData<Student>()
//        loadStudent()
//        return@lazy liveData
        MutableLiveData<Student>().also {
            loadStudent()
        }
    }


    private fun loadStudent() {
//        //加载数据
//        Thread(
//            Runnable {
//                Thread.sleep(1000)
//                student.postValue(Student("小美女",5))
//            }
//        ).start()

        //加载数据  协程
        GlobalScope.launch(Dispatchers.Main) {
            delay(1000)
            student.postValue(Student("小美女", 5))
        }
    }

    fun getStudent(): LiveData<Student> {
        return student
    }

}