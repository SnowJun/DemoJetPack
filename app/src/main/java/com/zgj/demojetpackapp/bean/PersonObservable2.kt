package com.zgj.demojetpackapp.bean

import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

/**
 *  @author Simple
 *  @date 2020/1/20
 *  @description ：
 **/
class PersonObservable2{

    var name:ObservableField<String> = ObservableField()
    var age:ObservableInt = ObservableInt()
    var car:ObservableArrayMap<String,Double> = ObservableArrayMap()


}
