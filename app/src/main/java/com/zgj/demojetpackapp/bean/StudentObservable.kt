package com.zgj.demojetpackapp.bean

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

/**
 *  @author Simple
 *  @date 2020/1/20
 *  @description ：
 **/
class StudentObservable(): BaseObservable(){

    constructor(name:String,age:Int):this(){
        this.name = name
        this.age = age
    }

    @Bindable
    var name:String? = null
    set(value) {
        field = value
        notifyPropertyChanged(BR.name)
        notifyChange()
    }
    @Bindable
    var age:Int? = null
    set(value) {
        field = value
        notifyPropertyChanged(BR.age)
    }


}