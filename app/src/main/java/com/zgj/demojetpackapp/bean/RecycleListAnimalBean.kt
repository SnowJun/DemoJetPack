package com.zgj.demojetpackapp.bean

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

/**
 *  @author Simple
 *  @date 2020/5/11
 *  @description ：
 **/
class RecycleListAnimalBean {

    /**
     * 名称、类型、年龄、描述
     */
    val name:ObservableField<String> = ObservableField()
    val type:ObservableInt = ObservableInt()
    val age:ObservableInt = ObservableInt()
    val typeDesc:ObservableField<String> = ObservableField()

}