package com.zgj.demojetpackapp

import android.app.Application
import android.content.Context

/**
 *  @author Simple
 *  @date 2020/1/13
 *  @description ：
 **/
class App :Application(){

    companion object{
        var context:Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }



}