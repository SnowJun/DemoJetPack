package com.zgj.demojetpackapp.lifecycles

import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.zgj.demojetpackapp.Common

/**
 *  @author Simple
 *  @date 2020/1/7
 *  @description ：
 **/
class MyObserver3: DefaultLifecycleObserver {

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        Log.d(Common.TAG,"MyObserver3onCreate->${owner.lifecycle.currentState}")
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        Log.d(Common.TAG,"MyObserver3onResume->${owner.lifecycle.currentState}")
    }

    override fun onPause(owner: LifecycleOwner) {
        super.onPause(owner)
        Log.d(Common.TAG,"MyObserver3onPause->${owner.lifecycle.currentState}")
    }

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        Log.d(Common.TAG,"MyObserver3onStart->${owner.lifecycle.currentState}")
    }

    override fun onStop(owner: LifecycleOwner) {
        super.onStop(owner)
        Log.d(Common.TAG,"onStop->${owner.lifecycle.currentState}")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        Log.d(Common.TAG,"MyObserver3onDestroy->${owner.lifecycle.currentState}")
    }
}