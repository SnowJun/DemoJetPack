package com.zgj.demojetpackapp.lifecycles

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.zgj.demojetpackapp.Common


/**
 *  @author Simple
 *  @date 2020/1/6
 *  @description ：
 **/
class MyObserver2: LifecycleEventObserver {


    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        Log.d(Common.TAG,"eventName->${event.name}")
    }

}