package com.zgj.demojetpackapp.lifecycles

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.zgj.demojetpackapp.Common

/**
 *  @author Simple
 *  @date 2020/1/6
 *  @description ：
 **/
class MyObserver:LifecycleObserver{


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
     fun resumeEvent() {
        Log.d(Common.TAG,"createEvent")
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
     fun pauseEvent() {
        Log.d(Common.TAG,"pauseEvent")
    }

}