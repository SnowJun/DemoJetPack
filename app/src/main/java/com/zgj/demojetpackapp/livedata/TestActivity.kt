package com.zgj.demojetpackapp.livedata

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R

import kotlinx.android.synthetic.main.activity_test_live_data.*

/**
 *  @author Simple
 *  @date 2020/1/6
 *  @description ：
 **/
class TestActivity : AppCompatActivity() {

    private lateinit var liveData: MutableLiveData<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_live_data)
        liveData = MutableLiveData()
        val statusObserver = Observer<String> {
            Log.d(Common.TAG, it)
        }
        liveData.observe(this, statusObserver)
        liveData.value = "onCreate()"

        tv_action.setOnClickListener {
            liveData.value = "点击按钮"
        }
    }


    override fun onStart() {
        super.onStart()
        Thread(Runnable {
            liveData.postValue( "onStart()")
        }).start()

    }

    override fun onResume() {
        super.onResume()
        liveData.postValue( "onResume()")
    }


    override fun onPause() {
        super.onPause()
        liveData.value = "onPause()"
    }

    override fun onStop() {
        super.onStop()
        liveData.value = "onStop()"
    }

    override fun onDestroy() {
        super.onDestroy()
        liveData.value = "onDestroy()"
    }


}