package com.zgj.demojetpackapp.databinding

import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.*
import com.zgj.demojetpackapp.BR
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.bean.Student

class TestActivity : AppCompatActivity() {

    var binding: ActivityTestDataBindingBinding? = null
    var student: Student = Student("小李", 23)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_test_data_binding
        )
        binding?.setVariable(BR.student,student)
//        binding?.listener = EventListener()
        binding?.setVariable(BR.listener, EventListener())

        binding?.list = listOf("jack","tom","wang")

        binding?.map  = mutableMapOf(Pair("name1","jack"), Pair("name2","tom"),Pair("name3","wang"))
        binding?.name1 = "name1"
        binding?.name2 = "name2"
        binding?.name33 = "name3"
        binding?.name4 = "name4"

        val sparse:SparseArray<String> = SparseArray<String>()
        sparse.put(1,"jack")
        sparse.put(2,"tom")
        sparse.put(3,"wang")
        binding?.sparse = sparse
    }


    inner class EventListener {
        fun onClick(view: View) {
            Toast.makeText(this@TestActivity, "点击按钮", Toast.LENGTH_SHORT).show()
        }

        fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            Log.d(Common.TAG, s.toString())
            binding?.student?.name = s.toString()
            binding?.student = student
        }

        fun customClickListener(student: Student) {
            Toast.makeText(this@TestActivity, student.name, Toast.LENGTH_SHORT).show()
        }
    }

}
