package com.zgj.demojetpackapp.databinding

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.bean.RecycleListAnimalBean

/**
 *  @author Simple
 *  @date 2020/5/11
 *  @description ：
 **/
class TestActivity4() : AppCompatActivity() {

    var binding: ActivityTestDataBinding4Binding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_data_binding4)
        init()
    }

    private fun init() {
        binding?.rvContent?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val animalBean = RecycleListAnimalBean()
        animalBean.name.set("老虎")
        animalBean.age.set(10)
        animalBean.type.set(1)
        animalBean.typeDesc.set("老虎是一种动物，和猫比较像")

        val animalBean1 = RecycleListAnimalBean()
        animalBean1.name.set("长颈鹿")
        animalBean1.age.set(20)
        animalBean1.type.set(2)
        animalBean1.typeDesc.set("长颈鹿是一种动物，脖子非常长")
        val data: MutableList<RecycleListAnimalBean> = mutableListOf(animalBean, animalBean1)

        var adapter = RecycleDemoAdapter()
        adapter.data = data
        binding?.rvContent?.adapter = adapter

    }

}