package com.zgj.demojetpackapp.databinding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.*
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.bean.PersonObservable2

class TestActivity3 : AppCompatActivity() {

    val person = PersonObservable2()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val binding: CustomBinding =
//            DataBindingUtil.setContentView(this, R.layout.activity_test_data_binding3)

        //和上述设定xml等效
        val binding: CustomBinding =
            CustomBinding.inflate(layoutInflater)
        setContentView(binding.root)


        person.name.set("张三")
        person.age.set(23)
        person.car["BYD"] = 10.0
        person.car["BM"] = 30.0
        person.car["AD"] = 50.0
        binding.person = person
        binding.listener = EventListener()
    }


    inner class EventListener {
        fun onClick(view: View) {
            person.age?.set(person.age.get().inc())
            person.car["TEST"] = 100.0
        }
    }


}
