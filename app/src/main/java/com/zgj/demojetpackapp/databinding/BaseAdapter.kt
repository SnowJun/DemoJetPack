package com.zgj.demojetpackapp.databinding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

/**
 *  @author Simple
 *  @date 2020/5/11
 *  @description ：
 **/
abstract class BaseAdapter<T>(var data :MutableList<T> = mutableListOf()) : RecyclerView.Adapter<BindingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder {
        return BindingViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                getLayoutId(),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return  data.size
    }


    abstract fun getLayoutId(): Int

}