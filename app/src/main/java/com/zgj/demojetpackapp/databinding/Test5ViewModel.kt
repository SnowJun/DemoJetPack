package com.zgj.demojetpackapp.databinding

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zgj.demojetpackapp.bean.Student
import com.zgj.demojetpackapp.bean.StudentObservable

/**
 *  @author Simple
 *  @date 2020/5/11
 *  @description ：
 **/
class Test5ViewModel : ViewModel() {


    init {
        Thread(Runnable {
            Thread.sleep(1000)
            val student = Student("李四", 20)
            studentData.postValue(student)

            studentObservable.name = "赵六"
            studentObservable.age = 60
        }).start()
    }

    val student: Student = Student("张三", 23)

    var studentData: MutableLiveData<Student> = MutableLiveData(student)

    val studentObservable: StudentObservable = StudentObservable("王五", 16)

}