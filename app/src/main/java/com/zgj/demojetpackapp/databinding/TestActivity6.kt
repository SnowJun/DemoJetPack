package com.zgj.demojetpackapp.databinding

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R

/**
 *  @author Simple
 *  @date 2020/5/11
 *  @description ：
 **/
class TestActivity6 : AppCompatActivity() {

    var binding: ActivityTestDataBinding6Binding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_data_binding6)
        binding?.lifecycleOwner = this

        val model: Test6ViewModel = ViewModelProviders.of(this).get(Test6ViewModel::class.java)

        binding?.nameViewModel = model

    }

}