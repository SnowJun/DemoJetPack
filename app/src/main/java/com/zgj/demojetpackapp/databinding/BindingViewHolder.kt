package com.zgj.demojetpackapp.databinding

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 *  @author Simple
 *  @date 2020/5/8
 *  @description ：
 **/
class BindingViewHolder(var dataBinding:ViewDataBinding) :RecyclerView.ViewHolder(dataBinding.root){
}