package com.zgj.demojetpackapp.databinding

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.databinding.BindingConversion
import java.text.SimpleDateFormat
import java.util.*

/**
 *  @author Simple
 *  @date 2020/5/19
 *  @description ：
 **/
object BindingConverts {

    @BindingConversion
    @JvmStatic
    fun convertDateToString(date: Date): String {
        return SimpleDateFormat.getDateInstance(0).format(date)
    }

    @BindingConversion
    @JvmStatic
    fun convertBooleanToDrawable(back: Boolean): Drawable {
        return if (back) {
            ColorDrawable(Color.RED)
        } else {
            ColorDrawable(Color.DKGRAY)
        }
    }

}