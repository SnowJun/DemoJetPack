package com.zgj.demojetpackapp.databinding

import androidx.lifecycle.MutableLiveData
import com.zgj.demojetpackapp.bean.Student

/**
 *  @author Simple
 *  @date 2020/5/11
 *  @description ：
 **/
class Test6ViewModel : ObservableViewModel() {


    init {
        Thread(Runnable {
            Thread.sleep(2000)
            student.name = "李四"
            student.age = 33
            notifyChange()

        }).start()
    }
    val student: Student = Student("张三", 23)

}