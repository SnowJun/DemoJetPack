package com.zgj.demojetpackapp.databinding

import com.zgj.demojetpackapp.BR
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.bean.RecycleListAnimalBean

/**
 *  @author Simple
 *  @date 2020/5/11
 *  @description ：
 **/
class RecycleDemoAdapter() : BaseAdapter<RecycleListAnimalBean>() {


    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
//        holder.dataBinding.setVariable(BR.animal,data.get(position))
        (holder.dataBinding as ItemRecycleBinding).animal = data[position]
    }



    override fun getLayoutId(): Int {
        return R.layout.item_recycle
    }
}