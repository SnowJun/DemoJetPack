package com.zgj.demojetpackapp.databinding

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.bean.ImgBean
import com.zgj.demojetpackapp.bean.Student
import java.text.SimpleDateFormat
import java.util.*

/**
 *  @author Simple
 *  @date 2020/5/19
 *  @description ：
 **/
class TestActivity7 : AppCompatActivity() {

    var binding: ActivityTestDataBinding7Binding? = null
    val handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_test_data_binding7
        )
        binding?.student = Student("小美女", 5)
        binding?.listener = EventListener()
        val url =
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1589877841442&di=55353d36cb62454230a0b86e90bd0c36&imgtype=0&src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F2018-11-06%2F5be15b7635e5d.jpg"
        binding?.imgBean = ImgBean(url, R.mipmap.error, R.mipmap.placeholder)
        binding?.info = "123456789"
        binding?.isException = false
        handler.postDelayed({
            binding?.isException = true
        }, 2000)
        binding?.date = Date()
    }

    companion object {
        @BindingAdapter(value = ["url", "error", "placeholder"], requireAll = false)
        @JvmStatic
        fun loadImg(view: ImageView, url: String, err: Int?, placeholder: Int?) {
            Log.d(Common.TAG, "加载图片：$url,err:$err,placeholder:$placeholder")
            Glide.with(view.context).load(url).also {
                placeholder?.let { it1 -> it.placeholder(it1) }
                err?.let { it1 -> it.error(it1) }
            }.into(view)
        }




    }


    inner class EventListener {
        fun onClick(view: View) {
            Toast.makeText(this@TestActivity7, "点击按钮", Toast.LENGTH_SHORT).show()
        }
    }

}