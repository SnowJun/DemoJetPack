package com.zgj.demojetpackapp.room.dao

import androidx.room.*
import com.zgj.demojetpackapp.room.po.Student

/**
 *  @author Simple
 *  @date 2020/1/10
 *  @description ：
 **/
@Dao
interface StudentDao {

    @Insert
    fun insert(student: Student)

    @Delete
    fun delete(student: Student)

    @Update
    fun update(student: Student)

    @Query("select * from Student")
    fun getStudentList(): MutableList<Student>
}