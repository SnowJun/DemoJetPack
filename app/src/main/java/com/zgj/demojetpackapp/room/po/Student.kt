package com.zgj.demojetpackapp.room.po

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *  @author Simple
 *  @date 2020/1/10
 *  @description ：
 **/
@Entity
data class Student (
    @PrimaryKey(autoGenerate = true)
    var id:Int?,
    var name:String?,
    var age:Int?
)