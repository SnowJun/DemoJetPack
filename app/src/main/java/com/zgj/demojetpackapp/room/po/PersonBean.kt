package com.zgj.demojetpackapp.room.po

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

/**
 *  @author Simple
 *  @date 2020/1/9
 *  @description ：
 **/
//@Entity(tableName = "tb_person")
@Entity
class PersonBean constructor(){

    constructor(name:String,age:Int):this(){
        this.name = name
        this.age = age
    }

    @PrimaryKey(autoGenerate = true)
    var id:Int? = null
    var name:String? = null
    var age:Int? = null

    @Ignore
    var sex:Int?=null

    override fun toString(): String {
        return "PersonBean(id=$id, name=$name, age=$age, sex=$sex)"
    }


}
//@Entity(tableName = "tb_person")
//data class Person (
//    @PrimaryKey(autoGenerate = true)
//    var id:Int?,
//    var name:String?,
//    var age: Int?
//)



