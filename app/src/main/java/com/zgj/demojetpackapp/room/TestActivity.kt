package com.zgj.demojetpackapp.room

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.zgj.demojetpackapp.Common
import com.zgj.demojetpackapp.R
import com.zgj.demojetpackapp.room.po.PersonBean
import com.zgj.demojetpackapp.room.po.Student

/**
 *  @author Simple
 *  @date 2020/1/9
 *  @description ：
 **/
class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_room)
        roomTest()
    }

    private fun roomTest() {
        val personDao = PersonRoomDatabase.getPersonRoomDatabase(this).personDao()


//        personDao.insertQuery(null,"小美女",12)
//
//        personDao.deleteQuery(2)

//        personDao.updateQuery(3,"大美女",22)

//        personDao.updateQuery()

//        val personBean = PersonBean("小陈",23)
//        personBean.id = 2
//        personDao.update(personBean)
//
//        val person = PersonBean("张大爷",80)
//        personDao.insert(person)
//        Log.d(Common.TAG,"插入数据：$person")
//        val person1 = PersonBean("小张",8)
//        personDao.insert(person1)
//        Log.d(Common.TAG,"插入数据：$person1")


        val person = PersonBean("张大爷", 80)

        val person1 = PersonBean("张大妈", 79)
        personDao.insert(person,person1)
        Log.d(Common.TAG, "插入数据：$person")
        Log.d(Common.TAG, "插入数据：$person1")

        var personList: MutableList<PersonBean> = personDao.getPersonList()
        Log.d(Common.TAG, "共有${personList.size}个人")
        personList.forEach {
            Log.d(Common.TAG, it.toString())
        }
    }

}