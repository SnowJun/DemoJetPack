package com.zgj.demojetpackapp.room.dao

import androidx.room.*
import com.zgj.demojetpackapp.room.po.PersonBean

/**
 *  @author Simple
 *  @date 2020/1/9
 *  @description ：
 **/
@Dao
interface PersonDao {

    @Insert
    fun insert(vararg person: PersonBean)

    @Delete
    fun delete(person: PersonBean)

    @Update
    fun update(person: PersonBean)


    @Query("SELECT * FROM PersonBean")
    fun getPersonList(): MutableList<PersonBean>


    @Query("select * from PersonBean where id= :id")
    fun findById(id:Int?): MutableList<PersonBean>

    @Query("select * from PersonBean where age > :age")
    fun findByAge(age:Int?): MutableList<PersonBean>

    @Query("select * from PersonBean where name like :name")
    fun findByNameRule(name: String?): MutableList<PersonBean>


    @Query("insert into PersonBean values (:id,:name,:age)")
    fun insertQuery(id:Int?,name:String?,age:Int?)

    @Query("delete from PersonBean where id = :id")
    fun deleteQuery(id:Int?)

    @Query("update PersonBean set name = :name , age = :age where id= :id")
    fun updateQuery(id:Int?,name:String?,age:Int?)

}